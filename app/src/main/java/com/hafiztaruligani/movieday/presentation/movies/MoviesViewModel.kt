package com.hafiztaruligani.movieday.presentation.movies

import android.os.Parcelable
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hafiztaruligani.cryptoday.util.Resource
import com.hafiztaruligani.movieday.domain.model.GenreWithMovies
import com.hafiztaruligani.movieday.domain.usecase.GetGenresWithMovies
import com.hafiztaruligani.movieday.presentation.CommonUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val getGenresWithMovies: GetGenresWithMovies
) : ViewModel() {
    private val _uiState = MutableStateFlow( MoviesUiState(loading = true) )
    val uiState : StateFlow<MoviesUiState> = _uiState

    val scrollState = MutableStateFlow(mutableMapOf<Int, Parcelable?>())

    init {
        initiateData()
    }

    fun initiateData() {
        viewModelScope.launch {

            getGenresWithMovies.invoke().collect(){
                when(it){
                    is Resource.Success -> {
                        _uiState.value = MoviesUiState(data = it.data)
                    }
                    is Resource.Error -> {
                        _uiState.value = MoviesUiState(error = it.message, data = it.data?: listOf())
                    }
                    is Resource.Loading -> {
                        _uiState.value = MoviesUiState(loading = true)
                    }
                }
            }

        }
    }

}

data class MoviesUiState (
    val loading : Boolean = false,
    val error : String = "",
    val data: List<GenreWithMovies>? = null
) {
    fun errorAndNoData() =
        error.isNotBlank() && (data?.isEmpty() == true || data == null )

}